package io.gitlab.alpertoy.restapi.repos;

import io.gitlab.alpertoy.restapi.entities.Client;
import io.gitlab.alpertoy.restapi.entities.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query("from Region")
    List<Region> findAllRegions();

    Optional<Client> findByEmail(String email);

}
