package io.gitlab.alpertoy.restapi.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@Data
public class ClientDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -3329836025255319389L;

    private Long id;

    @NotEmpty(message = "First name should not be empty")
    @Size(min = 4, max = 12, message = "First name should contain min 4 and max 12 characters")
    private String firstName;

    @NotEmpty(message = "Last name should not be empty")
    private String lastName;

    @NotEmpty(message = "Email should not be empty")
    @Email(message = "Email should be valid")
    private String email;

    @NotNull(message = "Date should not be null")
    private Date createdAt;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private RegionDTO region;
}
