package io.gitlab.alpertoy.restapi.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@AllArgsConstructor
@Data
public class RegionDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -4164585539763130750L;

    private Long id;

    private String name;
}
