package io.gitlab.alpertoy.restapi.dtos;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class UserDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -7841204892127638808L;

    private String username;
    private String password;
    
}
