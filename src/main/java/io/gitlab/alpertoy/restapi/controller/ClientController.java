package io.gitlab.alpertoy.restapi.controller;

import io.gitlab.alpertoy.restapi.dtos.ClientDTO;
import io.gitlab.alpertoy.restapi.dtos.RegionDTO;
import io.gitlab.alpertoy.restapi.services.IClientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Clients")
@RestController
@RequestMapping("/api")
public class ClientController {

    private final IClientService clientService;

    public ClientController(IClientService clientService) {
        this.clientService = clientService;
    }

    @Operation(summary = "List all clients")
    @GetMapping("/clients")
    public ResponseEntity<List<ClientDTO>> findAll() {
        List<ClientDTO> clients = clientService.findAll();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @Operation(summary = "Find client by id")
    @GetMapping("/clients/{id}")
    public ResponseEntity<ClientDTO> findById(@Valid @PathVariable("id") Long id) {
        ClientDTO client = clientService.findById(id);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @Operation(summary = "Add client")
    @PostMapping("/clients")
    public ResponseEntity<ClientDTO> save(@Valid @RequestBody ClientDTO client) {
        ClientDTO savedClient = clientService.save(client);
        return new ResponseEntity<>(savedClient, HttpStatus.CREATED);
    }

    @Operation(summary = "Update client")
    @PutMapping("/clients/{id}")
    public ResponseEntity<ClientDTO> update(@PathVariable("id") Long id, @RequestBody @Valid ClientDTO client) {
        client.setId(id);
        ClientDTO updatedClient = clientService.update(client);
        return new ResponseEntity<>(updatedClient, HttpStatus.OK);
    }

    @Operation(summary = "Delete client")
    @DeleteMapping("/clients/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        clientService.delete(id);
        return new ResponseEntity<>("Client has been deleted", HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "List all regions")
    @GetMapping("/clients/regions")
    public ResponseEntity<List<RegionDTO>> listRegions() {
        List<RegionDTO> regions = clientService.findAllRegions();
        return new ResponseEntity<>(regions, HttpStatus.OK);
    }
}
