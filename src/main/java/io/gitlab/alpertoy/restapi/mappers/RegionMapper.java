package io.gitlab.alpertoy.restapi.mappers;

import io.gitlab.alpertoy.restapi.dtos.RegionDTO;
import io.gitlab.alpertoy.restapi.entities.Region;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RegionMapper {

    RegionMapper MAPPER = Mappers.getMapper(RegionMapper.class);

    RegionDTO mapToRegionDTO(Region region);

    Region mapToRegion(RegionDTO regionDTO);
}
