package io.gitlab.alpertoy.restapi.mappers;

import io.gitlab.alpertoy.restapi.dtos.ClientDTO;
import io.gitlab.alpertoy.restapi.entities.Client;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClientMapper {

    ClientMapper MAPPER = Mappers.getMapper(ClientMapper.class);

    ClientDTO mapToClientDTO(Client client);

    Client mapToClient(ClientDTO clientDTO);
}
