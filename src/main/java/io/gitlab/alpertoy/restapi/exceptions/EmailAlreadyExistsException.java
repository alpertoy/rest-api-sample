package io.gitlab.alpertoy.restapi.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class EmailAlreadyExistsException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 6568973821534459859L;

    private String message;

    public EmailAlreadyExistsException(String message) {
        super(message);
    }
}
