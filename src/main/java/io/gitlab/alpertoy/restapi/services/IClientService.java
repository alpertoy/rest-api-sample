package io.gitlab.alpertoy.restapi.services;

import io.gitlab.alpertoy.restapi.dtos.ClientDTO;
import io.gitlab.alpertoy.restapi.dtos.RegionDTO;

import java.util.List;

public interface IClientService {

    List<ClientDTO> findAll();

    ClientDTO findById(Long id);

    ClientDTO save(ClientDTO clientDTO);

    void delete(Long id);

    ClientDTO update(ClientDTO client);

    List<RegionDTO> findAllRegions();
}
