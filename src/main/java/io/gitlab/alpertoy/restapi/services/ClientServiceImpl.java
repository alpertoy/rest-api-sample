package io.gitlab.alpertoy.restapi.services;

import io.gitlab.alpertoy.restapi.dtos.ClientDTO;
import io.gitlab.alpertoy.restapi.dtos.RegionDTO;
import io.gitlab.alpertoy.restapi.entities.Client;
import io.gitlab.alpertoy.restapi.entities.Region;
import io.gitlab.alpertoy.restapi.exceptions.EmailAlreadyExistsException;
import io.gitlab.alpertoy.restapi.exceptions.ResourceNotFoundException;
import io.gitlab.alpertoy.restapi.mappers.ClientMapper;
import io.gitlab.alpertoy.restapi.mappers.RegionMapper;
import io.gitlab.alpertoy.restapi.repos.ClientRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements IClientService {

    // Constructor type injection
    private final ClientRepository repository;

    public ClientServiceImpl(ClientRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional(readOnly = true)
    @CachePut(cacheNames = "clients")
    public List<ClientDTO> findAll() {
        List<Client> clients = (List<Client>) repository.findAll();
        return clients.stream().map(ClientMapper.MAPPER::mapToClientDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    @CachePut(cacheNames = "clients", key="#id")
    public ClientDTO findById(Long id) {
        Client client = repository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Client", "id", id)
        );
        return ClientMapper.MAPPER.mapToClientDTO(client);
    }

    @Override
    @Transactional
    public ClientDTO save(ClientDTO clientDTO) {
        Client client = ClientMapper.MAPPER.mapToClient(clientDTO);
        Optional<Client> optionalClient = repository.findByEmail(clientDTO.getEmail());
        if(optionalClient.isPresent()) {
            throw new EmailAlreadyExistsException("Email already exists!");
        }
        Client savedClient = repository.save(client);
        return ClientMapper.MAPPER.mapToClientDTO(savedClient);
    }

    @Override
    @Transactional
    @CacheEvict(cacheNames = "clients", key = "#id")
    public void delete(Long id) {
        Client existingClient = repository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Client", "id", id)
        );
        repository.deleteById(id);
    }

    @Override
    @Transactional
    @CachePut(cacheNames = "clients", key="#client")
    public ClientDTO update(ClientDTO client) {
        Client existingClient = repository.findById(client.getId()).orElseThrow(
                () -> new ResourceNotFoundException("Client", "id", client.getId())
        );

        existingClient.setFirstName(client.getFirstName());
        existingClient.setLastName(client.getLastName());
        existingClient.setEmail(client.getEmail());
        existingClient.setCreatedAt(client.getCreatedAt());
        Client updatedClient = repository.save(existingClient);
        return ClientMapper.MAPPER.mapToClientDTO(updatedClient);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable(cacheNames = "regions")
    public List<RegionDTO> findAllRegions() {
        List<Region> regions = (List<Region>) repository.findAllRegions();
        return regions.stream().map(RegionMapper.MAPPER::mapToRegionDTO)
                .collect(Collectors.toList());
    }
}
