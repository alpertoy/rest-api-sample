INSERT INTO regions (id, name) VALUES (1, 'Europe');
INSERT INTO regions (id, name) VALUES (2, 'Asia');
INSERT INTO regions (id, name) VALUES (3, 'North America');
INSERT INTO regions (id, name) VALUES (4, 'Ocenia');

INSERT INTO clients (region_id, first_name, last_name, email, created_at) VALUES(1, 'Alper', 'Toy', 'alpertoy@gmail.com', NOW());
INSERT INTO clients (region_id, first_name, last_name, email, created_at) VALUES(2, 'John', 'Doe', 'johndoe@gmail.com', NOW());
INSERT INTO clients (region_id, first_name, last_name, email, created_at) VALUES(3, 'Peter', 'Oak', 'peteroak@gmail.com', NOW());